# PowerLine

Sources and configurations to enable a powerline build for fish-shell<br />
[github repo](https://github.com/0rax/fishline)
